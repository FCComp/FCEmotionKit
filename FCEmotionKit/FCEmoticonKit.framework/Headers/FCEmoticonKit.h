//
//  FCEmoticonKit.h
//  FCEmoticonKit
//
//  Created by omni－appple on 2018/12/13.
//  Copyright © 2018年 ZhouYou. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for FCEmoticonKit.
FOUNDATION_EXPORT double FCEmoticonKitVersionNumber;

//! Project version string for FCEmoticonKit.
FOUNDATION_EXPORT const unsigned char FCEmoticonKitVersionString[];
#import <FCEmoticonKit/FCEmoticon.h>
#import <FCEmoticonKit/FCEmoticonTextView.h>
#import <FCEmoticonKit/FCEmoticonManager.h>

// In this header, you should import all the public headers of your framework using statements like #import <FCEmoticonKit/PublicHeader.h>


